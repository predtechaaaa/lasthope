#ifndef FILTER_H
#define FILTER_H
#include "conference-program.h"

conference_program** filter(conference_program* array[], int size, bool (*check)(conference_program* element), int& result_size);
bool check_by_author(conference_program* element);
bool check_by_time(conference_program* element);
#endif
