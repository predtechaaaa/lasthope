#ifndef PROCESSING_H
#define PROCESSING_H

#include "conference-program.h"
#include <iostream>
#include <string>
#include <sstream>
using namespace std;
string process(conference_program* array[], int size);

#endif
