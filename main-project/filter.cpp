#include "filter.h"
#include <cstring>
#include <iostream>
#include "conference-program.h"
conference_program** filter(conference_program* array[], int size, bool (*check)(conference_program* element), int& result_size)
{
	conference_program** result = new conference_program * [size];
	result_size = 0;
	for (int i = 0; i < size; i++)
	{
		if (check(array[i]))
		{
			result[result_size++] = array[i];
		}
	}
	return result;
}

bool check_by_author(conference_program* element)
{
	return strcmp(element->reader.last_name, "������") == 0 &&
		strcmp(element->reader.first_name, "����") == 0 &&
		strcmp(element->reader.middle_name, "��������") == 0;
}
bool check_by_time(conference_program* element)
{
	if (element->finish.minute - element->start.minute > 15)
	{
		return 1;
	}
	else
	{
		return 0;
	}
}
