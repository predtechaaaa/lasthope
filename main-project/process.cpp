#ifndef PROCESS_H
#define PROCESS_H

#include "processing.h"
#include "conference-program.h"
#include <iostream>
#include <string>
#include <sstream>


int diff(int a, int b)
{
	return (a > b ? a - b : a - b);
}

string process(conference_program* array[], int size)
{
	int max = diff(array[0]->finish.hour, array[0]->start.hour);
	int maxm = diff(array[0]->finish.hour, array[0]->start.minute);
	for (int i = 1; i < size; i++)
	{
		int curr = diff(array[i]->finish.hour, array[i]->start.hour);
		int currm = diff(array[i]->finish.minute, array[i]->start.minute);
		if (curr > max)
		{
			max = curr;
			maxm = currm;
		}
		else if (curr == max)
		{
			if (currm > maxm)
			{
				max = curr;
				maxm = currm;
			}
		}
	}
	stringstream ss;
	ss << max << ":" << maxm;

	string result;
	ss >> result;
	return result;
}


#endif