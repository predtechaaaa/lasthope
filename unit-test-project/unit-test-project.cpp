#include "pch.h"
#include "CppUnitTest.h"
#include "../main-project/conference-program.h"
#include "../main-project/processing.h"
using namespace Microsoft::VisualStudio::CppUnitTestFramework;

conference_program* build(int start_hour, int start_minute, int finish_hour, int finish_minute)
{
	conference_program* program = new conference_program;
	program->start.hour = start_hour;
	program->start.minute = start_minute;
	program->finish.hour = finish_hour;
	program->finish.minute = finish_minute;
	return program;
}

//       
void delete_program(conference_program* array[], int size)
{
	for (int i = 0; i < size; i++)
	{
		delete array[i];
	}
}


namespace UnitTest1
{
	TEST_CLASS(UnitTest1)
	{
	public:

		TEST_METHOD(TestMethod1)
		{
			conference_program* program[3];
			string s = { "3:10" };
			program[0] = build(9, 10, 10, 20); // 5 
			program[1] = build(9, 30, 12, 40); // 7 
			program[2] = build(7, 10, 8, 30); // 4 
			Assert::AreEqual(s, process(program, 3));
			delete_program(program, 3);
		}
		TEST_METHOD(TestMethod2)
		{
			conference_program* program[3];
			string s = { "1:10" };
			program[0] = build(9, 10, 9, 20); // 5  
			program[1] = build(9, 30, 10, 00); // 7 
			program[2] = build(10, 05, 11, 15); // 4 
			Assert::AreEqual(s, process(program, 3));
			delete_program(program, 3);
		}
		TEST_METHOD(TestMethod3)
		{
			conference_program* program[3];
			string s = { "2:10" };
			program[0] = build(12, 10, 12, 55); // 5 
			program[1] = build(13, 30, 15, 00); // 7
			program[2] = build(16, 05, 18, 15); // 4
			Assert::AreEqual(s, process(program, 3));
			delete_program(program, 3);
		}
	};
}
